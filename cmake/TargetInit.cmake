include(cmake/build/CompilerWarnings.cmake)

function(target_init project_name)

  set_target_properties(${project_name}
    PROPERTIES
      CXX_STANDARD 14
      CXX_STANDARD_REQUIRED YES
      CXX_EXTENSIONS NO
  )

  # Standard compiler warnings
  #set_project_warnings(${project_name})

endfunction()
